class Ave:
    def volar(self):
        raise NotImplementedError("Subclase debe implementar este método")
# Subclase que cumple con LSP
class Pajaro(Ave):
    def volar(self):print("El pájaro vuela")
# Subclase que no cumple con LSP porque no puede volar
class Pinguino(Ave):
    def volar(self):
        raise Exception("Los pingüinos no pueden volar")
# Función que acepta cualquier tipo de Ave
def hacer_volar(ave):
    ave.volar()
# Pruebas
pajaro = Pajaro()
hacer_volar(pajaro) # Output: "El pájaro vuela"
# Esto rompe LSP porque los pingüinos no pueden volar
#pinguino = Pinguino()
#hacer_volar(pinguino) # Output: Exception raised