# Definir interfaces específicas
class Impresora:
    def imprimir(self):
        raise NotImplementedError("Subclase debe implementar este método")
class Escaner:
    def escanear(self):
        raise NotImplementedError("Subclase debe implementar este método")
class Copiar:
    def copiar(self):
        raise NotImplementedError("Subclase debe implementar este método")
    
# Clase que implementa ambas interfaces
class Multifuncion(Impresora, Escaner):
    def imprimir(self):
        print("Imprimiendo documento")
    def escanear(self):
        print("Escaneando documento")
    def copiar(self):
        print("Copiando documento")
# Pruebas
multifuncion = Multifuncion()
multifuncion.imprimir() # Output: "Imprimiendo documento"
multifuncion.escanear() # Output: "Escaneando documento"
multifuncion.copiar()